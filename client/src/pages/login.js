import React from "react";
import { Form, Input, Button } from "antd";

import axios from "../utils/axios-config";
import { customMessage } from "../components/message";

import "../assets/styles/forms.css";

const Login = ({ history, login }) => {
  const onFinish = async ({ email, password }) => {
    try {
      const response = await axios.post("/users/login", { email, password });
      const { token, email: resEmail, message, ok } = response.data;
      if (ok) {
        customMessage("success", message);
        setTimeout(() => {
          login(token, resEmail);
        }, 2000);
      }
    } catch (error) {
      customMessage("error", "Something went wrong");
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="form-main">
      <div className="form-container">
        <Form name="basic" onFinish={onFinish} onFinishFailed={onFinishFailed}>
          <div className="form-header">
            <span>Login</span>
          </div>
          <label>
            <span>*</span>Email
          </label>
          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                message: "Please input your email!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <label>
            <span>*</span>Password
          </label>

          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit" block>
              Submit
            </Button>
          </Form.Item>
          <div className="form-navigation">
            <span onClick={() => history.push("/register")}>
              Not a member yet?
            </span>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default Login;
