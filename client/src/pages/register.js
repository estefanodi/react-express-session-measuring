import React from "react";
import { Form, Input, Button } from "antd";

import axios from "../utils/axios-config";
import { customMessage } from "../components/message";

import "../assets/styles/forms.css";

const Register = ({ history, login }) => {
  const onFinish = async ({ email, password, confirmPassword }) => {
    try {
      const response = await axios.post("/users/register", {
        email,
        password,
        confirmPassword,
      });
      const { token, email: resEmail, message, ok } = response.data;
      customMessage("success", message);
      if (ok) {
        setTimeout(() => {
          login(token, resEmail);
        }, 2000);
      }
    } catch (error) {
      customMessage("error", "Something went wrong");
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="form-main">
      <div className="form-container">
        <Form name="basic" onFinish={onFinish} onFinishFailed={onFinishFailed}>
          <div className="form-header">
            <span>Register</span>
          </div>
          <label>
            <span>*</span>Email
          </label>
          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                message: "Please input an email!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <label>
            <span>*</span>Password
          </label>

          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Please input a password!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
          <label>
            <span>*</span>Confirm Password
          </label>

          <Form.Item
            name="confirmPassword"
            dependencies={["password"]}
            hasFeedback
            rules={[
              {
                required: true,
                message: "Please confirm your password!",
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }

                  return Promise.reject(
                    new Error(
                      "The two passwords that you entered do not match!"
                    )
                  );
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit" block>
              Submit
            </Button>
          </Form.Item>
          <div className="form-navigation">
            <span onClick={() => history.push("/login")}>Already member?</span>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default Register;
