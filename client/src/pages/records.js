import React from "react";
import { useRecoilValue, useRecoilState } from "recoil";
import { records, isLoggedIn, recordsCopy } from "../utils/state";
import { Button } from "antd";
import { UnorderedListOutlined } from "@ant-design/icons";

import TableComponent from "../components/table";
import DatePickerComponent from "../components/datepicker";

import axios from "../utils/axios-config";
import { calculateTotalTime } from "../utils/time-helper";
import { customMessage } from "../components/message";

import "../assets/styles/records.css";

const Records = () => {
  const isLoggedInState = useRecoilValue(isLoggedIn);
  const [recordsState, setRecordsState] = useRecoilState(records);
  const [recordsCopyState, setRecordsCopyState] = useRecoilState(recordsCopy);

  const getRecords = async () => {
    try {
      axios.defaults.headers.common["Authorization"] =
        localStorage.getItem("token");
      const response = await axios.get("/records/get-records");
      setRecordsState([...response.data.records]);
      setRecordsCopyState([...response.data.records]);
    } catch (error) {
      customMessage("error", "Something went wrong");
    }
  };

  React.useEffect(() => {
    if (isLoggedInState) {
      getRecords();
    }
  }, [isLoggedInState]);

  return (
    <div className="records-container">
      <div className="records-filters-container">
        <div>
          <DatePickerComponent />
          <Button
            onClick={() => setRecordsState(recordsCopyState)}
            className="display-all-btn"
            icon={<UnorderedListOutlined />}
          >
            Display All
          </Button>
        </div>
        <span className="length-counter">
          Total items {recordsState.length}
        </span>
      </div>
      <TableComponent />
      <div className="records-footer">
        <span className="length-counter">
          Total time {calculateTotalTime(recordsState)}
        </span>
      </div>
    </div>
  );
};

export default Records;
