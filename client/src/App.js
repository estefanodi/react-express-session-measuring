import React from "react";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import { useRecoilState, useSetRecoilState } from "recoil";
import { useBeforeunload } from "react-beforeunload";
//* =========  COMPONENTS  ===========
import Navbar from "./components/navbar";
import { customMessage } from "./components/message";
//* ==========  PAGES  ===============
import Register from "./pages/register";
import Login from "./pages/login";
import Records from "./pages/records";
//* ==================================
import axios from "./utils/axios-config";
import { saveRecord } from "./utils/records-actions";
import { isLoggedIn, email } from "./utils/state";

const App = () => {
  const setEmailState = useSetRecoilState(email);
  const [isLoggedInState, setIsLoggedInState] = useRecoilState(isLoggedIn);

  useBeforeunload(() => {
    handlePromise();
    return "You'll lose your data!";
  });

  const pendingOps = new Set();

  const handlePromise = () => {
    if (isLoggedInState) {
      axios.defaults.headers.common["Authorization"] =
        localStorage.getItem("token");
      const promise = axios.post("/records/save-record", {
        startTime: JSON.parse(localStorage.getItem("startTime")),
        endTime: new Date(),
      });
      pendingOps.add(promise);
      const cleanup = () => {
        localStorage.clear();
        return pendingOps.delete(promise);
      }
      return promise
          .then(cleanup)
          .catch(cleanup);
    }
  }

  const login = (token, email) => {
    localStorage.setItem("token", token);
    localStorage.setItem("startTime", JSON.stringify(new Date()));
    setIsLoggedInState(true);
    setEmailState(email);
  };

  const logout = async () => {
    try {
      await saveRecord();
      localStorage.removeItem("token");
      setIsLoggedInState(false);
      setEmailState("");
    } catch (error) {
      customMessage("error", "Something went wrong");
    }
  };

  return (
    <Router>
      {isLoggedInState && <Navbar logout={logout} />}
      <Route
        exact
        path="/"
        render={(props) =>
          !isLoggedInState ? <Redirect to={"/login"} /> : <Records {...props} />
        }
      />
      <Route
        exact
        path="/login"
        render={(props) =>
          isLoggedInState ? (
            <Redirect to={"/"} />
          ) : (
            <Login {...props} login={login} />
          )
        }
      />
      <Route
        path="/register"
        render={(props) =>
          isLoggedInState ? (
            <Redirect to={"/"} />
          ) : (
            <Register {...props} login={login} />
          )
        }
      />
    </Router>
  );
};

export default App;
