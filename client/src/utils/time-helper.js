import moment from "moment";

export const calculateTotalTime = (records) => {
  let hours = 0;
  let minutes = 0;
  let seconds = 0;
  const temp = [];
  for (let i = 0; i < records.length; i++) {
    temp.push(calculateDifference(records[i].startTime, records[i].endTime));
    const splitted = calculateDifference(
      records[i].startTime,
      records[i].endTime
    ).split(" : ");
    seconds += Number(splitted[2]);
    minutes += Number(splitted[1]);
    hours += Number(splitted[0]);
  }
  hours = parseInt(hours + minutes / 60);
  seconds = seconds < 60 ? seconds : seconds % 60;
  minutes = minutes % 60;
  minutes = minutes < 60 ? minutes : parseInt(minutes + seconds / 60);
  return `${addZero(hours)}:${addZero(minutes)}:${addZero(seconds)}`;
};

const getDifference = (recordStart, recordEnd) => {
  const startTime = moment(recordStart, "DD-MM-YYYY hh:mm:ss");
  const endTime = moment(recordEnd, "DD-MM-YYYY hh:mm:ss");
  const hoursDiff = endTime.diff(startTime, "hours");
  const minutesDiff = endTime.diff(startTime, "minutes");
  const secondsDiff = endTime.diff(startTime, "seconds");
  return { hoursDiff, minutesDiff, secondsDiff };
};

const addZero = (num) => (Number(num) < 10 ? "0" + num : num);

export const calculateDifference = (recordStart, recordEnd) => {
  const data = getDifference(recordStart, recordEnd);
  return `${addZero(data.hoursDiff)} : ${addZero(
    data.minutesDiff % 60
  )} : ${addZero(data.secondsDiff % 60)}`;
};
