import axios from 'axios';

export const baseURL = `http://localhost:3030`;

const customInstance = axios.create ({
  baseURL, 
  headers: {'Accept': 'application/json'}
})

export default customInstance;