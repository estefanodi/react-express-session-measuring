import axios from "./axios-config";

export const saveRecord = async () => {
  try {
    axios.defaults.headers.common["Authorization"] =
      localStorage.getItem("token");
    await axios.post("/records/save-record", {
      startTime: JSON.parse(localStorage.getItem("startTime")),
      endTime: new Date(),
    });
    localStorage.clear();
    return true;
  } catch (error) {
    console.log(error);
  }
};
