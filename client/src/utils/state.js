import { atom } from "recoil";

export const records = atom({
  key: "records",
  default: [],
});

export const recordsCopy = atom({
  key: "recordsCopy",
  default: [],
});

export const isLoggedIn = atom({
  key: "isLoggedIn",
  default: false,
});

export const email = atom({
  key: "email",
  default: "",
});
