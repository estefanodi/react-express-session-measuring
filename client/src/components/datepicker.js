import { DatePicker } from "antd";
import { useSetRecoilState } from "recoil";

import { customMessage } from "../components/message";
import { records } from "../utils/state";
import axios from "../utils/axios-config";

const { RangePicker } = DatePicker;

const DatePickerComponent = () => {
  const setRecordsState = useSetRecoilState(records);
  const filterRecords = async (dates) => {
    try {
      axios.defaults.headers.common["Authorization"] =
        localStorage.getItem("token");
      const response = await axios.post("/records/get-between-dates", dates);
      setRecordsState([...response.data.records]);
    } catch (error) {
      customMessage("error", "Something went wrong");
    }
  };
  return (
    <RangePicker
      className="datepicker"
      onChange={(val) => {
        if (!val) return;
        filterRecords({
          startDate: val[0]._d.setHours(0, 0, 0),
          endDate: val[1]._d.setHours(23, 59, 59),
        });
      }}
    />
  );
};

export default DatePickerComponent;
