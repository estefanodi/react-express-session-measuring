import { message } from 'antd';


export const customMessage = (type, text) => message[type](text);

