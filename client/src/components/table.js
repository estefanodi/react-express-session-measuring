import { Table } from "antd";
import moment from "moment";
import momentTimezone from "moment-timezone";
import { useRecoilValue } from "recoil";

import { calculateDifference } from "../utils/time-helper";
import { records } from "../utils/state";

const columns = [
  {
    title: "Record Date",
    dataIndex: "recordDate",
    key: "record date",
  },
  {
    title: "Record Initial",
    dataIndex: "startTime",
    key: "record initial",
  },
  {
    title: "Record Ending",
    dataIndex: "endTime",
    key: "record ending",
  },
  {
    title: "Record Total Time",
    dataIndex: "totalTime",
    key: "record total time",
  },
];

const objectModel = (oldObject) => {
  return {
    key: oldObject._id,
    recordDate: moment(oldObject.startTime).format("MMM Do YYYY"),
    startTime: momentTimezone(oldObject.startTime).tz("CET").format("HH:mm:ss"),
    endTime: momentTimezone(oldObject.endTime).tz("CET").format("HH:mm:ss"),
    totalTime: calculateDifference(oldObject.startTime, oldObject.endTime),
  };
};

const TableComponent = () => {
  let recordsState = useRecoilValue(records);
  return (
    <Table
      className="table"
      columns={columns}
      dataSource={recordsState.map((rc) => objectModel(rc))}
      pagination={false}
    />
  );
};

export default TableComponent;
