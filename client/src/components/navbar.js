import React from "react";
import { useRecoilValue } from "recoil";
import { Button } from "antd";

import { email } from "../utils/state";

const Navbar = ({ logout }) => {
  const emailState = useRecoilValue(email);
  return (
    <div className="navbar">
      <span>{emailState}</span>
      <Button htmlType="submit" onClick={() => logout()}>
        Logout
      </Button>
    </div>
  );
};

export default Navbar;
