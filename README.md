## HOW TO RUN IT 

Clone the repo running from the terminal 

```
git clone https://gitlab.com/estefanodi/users-session-measuring.git
```

From a terminal tab


```
cd user-session-measuring

cd client

npm i 

npm start
```

From a different terminal tab 

```
cd user-session-measuring

cd server

npm i

nodemon or node index.js
```

The project is on the branch master

## DESCRIPTION

The project is using React, React Router and Recoil ( as a state manager) on the client side and Express on the server, the database is running on Mongo Atlas.

The client has 3 pages: 

1) login page 

2) register page

3) records page ( a page where to read and filter all the user session records )

<img src="https://res.cloudinary.com/estefanodi2009/image/upload/v1628373378/Screenshot_from_2021-08-07_23-54-22.png" alt="login page">

<img src="https://res.cloudinary.com/estefanodi2009/image/upload/v1628373378/Screenshot_from_2021-08-07_23-54-26.png" alt="register page">

<img src="https://res.cloudinary.com/estefanodi2009/image/upload/v1628373378/Screenshot_from_2021-08-07_23-54-41.png" alt="records page">

## NOTES

I spent more or less 2 days to build it, the trickiest part was saving a record when the browser tab was closing, at the beginning I was using the 'beforeunload' and 'unload' event listeners, but eventually I decided to use a package called "react-beforeunload" as 'beforeunload' and 'unload' were not working as expected.