const router = require("express").Router();
const controller = require("../controllers/controllers.records");
const verifyToken = require("../middlewares/verify_token");

router.post("/save-record", verifyToken, controller.saveRecord);
router.get("/get-records", verifyToken, controller.getAllRecords);
router.post("/get-between-dates", verifyToken, controller.getBetweenDates);

module.exports = router;
