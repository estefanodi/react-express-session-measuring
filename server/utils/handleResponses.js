const handleResponse = (res, ok, code, message, data) => {
    return res.status(code).json({ok,message,...data});
} 

const createError = (next, code, message) => {
    const error = new Error(message);
    error.statusCode = code;
    next(error);
} 

module.exports = {
    handleResponse,
    createError
}