const User = require("../models/models.users");
const argon2 = require("argon2");
const jwt = require("jsonwebtoken");
const validator = require("validator");
const jwt_secret = process.env.JWT_SECRET;
const { handleResponse, createError } = require("../utils/handleResponses");

//* ==========================================================
//* =====================  REGISTER  =========================
//* ==========================================================

const register = async (req, res) => {
  const { email, password, confirmPassword } = req.body;
  if (!email || !password || !confirmPassword || !validator.isEmail(email))
    return createError(next, 422, "Unprocessable entity");
  if (password !== confirmPassword)
    return createError(next, 401, "Unauthorized");
  try {
    const hash = await argon2.hash(password);
    const newUser = {
      email,
      password: hash,
    };
    const created = await User.create(newUser);
    const tokenContent = {
      _id: created._id,
    };
    const token = jwt.sign(tokenContent, jwt_secret, { expiresIn: "7d" });
    return handleResponse(res, true, 200, "Successfully registered", {
      token,
      email,
    });
  } catch (error) {
    next(error);
  }
};

//* ==========================================================
//* ======================  LOGIN  ===========================
//* ==========================================================

const login = async (req, res, next) => {
  const { email, password } = req.body;
  if (!email || !password || !validator.isEmail(email))
    return createError(next, 422, "Unprocessable entity");
  try {
    const user = await User.findOne({ email });
    if (!user) return createError(next, 404, "Resource not found");
    const match = await argon2.verify(user.password, password);
    if (match) {
      const tokenContent = {
        _id: user._id,
      };
      const token = jwt.sign(tokenContent, jwt_secret, { expiresIn: "7d" });
      return handleResponse(res, true, 200, "Welcome back", {token, email});
    } else return createError(next, 422, "Unprocessable entity");
  } catch (error) {
    next(error);
  }
};

module.exports = { register, login };
