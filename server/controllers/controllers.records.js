const Records = require("../models/models.records");
const { handleResponse, createError } = require("../utils/handleResponses");

//* ==========================================================
//* ==================  SAVE RECORD  =========================
//* ==========================================================

const saveRecord = async (req, res, next) => {
  const { startTime, endTime } = req.body;
  if (!startTime || !endTime)
    return createError(next, 422, "Unprocessable entity");
  try {
    await Records.create({
      userId: req.user._id,
      startTime,
      endTime,
    });
    return handleResponse(res, true, 200, "Entity created", null);
  } catch (error) {
    next(error);
  }
};

//* ==========================================================
//* ==================  GET ALL RECORDS  =====================
//* ==========================================================

const getAllRecords = async (req, res, next) => {
  try {
    const records = await Records.find({ userId: req.user._id }).sort({
      startTime: 1,
    });
    return handleResponse(res, true, 200, "Records found", { records });
  } catch (error) {
    next(error);
  }
};

//* ==========================================================
//* ==================  GET BETWEEN DATES  ===================
//* ==========================================================

const getBetweenDates = async (req, res, next) => {
  const { startDate, endDate } = req.body;
  if (!startDate || !endDate)
    return createError(next, 422, "Unprocessable entity");
  try {
    const records = await Records.find({
      userId: req.user.id,
      startTime: {
        $gte: startDate,
        $lte: endDate,
      },
    }).sort({ startTime: 1 });
    return handleResponse(res, true, 200, "Records found", { records });
  } catch (error) {
    next(error);
  }
};

module.exports = {
  saveRecord,
  getAllRecords,
  getBetweenDates
};
