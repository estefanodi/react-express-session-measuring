const express = require("express");
const app = express();
const mongoose = require("mongoose");
const port = process.env.PORT || 3030;
require("dotenv").config();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

async function connecting() {
  try {
    await mongoose.connect(process.env.MONGO, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });
    console.log("Connected to the DB");
  } catch (error) {
    console.log(error);
  }
}
connecting();

const cors = require("cors");
app.use(cors());

app.use("/users", require("./routes/routes.users"));
app.use("/records", require("./routes/routes.records"));

app.listen(port, () => {
  console.log(`Server connected on port ${port}`);
});
