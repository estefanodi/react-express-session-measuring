const jwt = require("jsonwebtoken");
const jwt_secret = process.env.JWT_SECRET;
const { createError } = require("../utils/handleResponses");
const Users = require("../models/models.users");

module.exports = async function verifyToken(req, _, next) {
  const token = req.headers.authorization;
  try {
    const verified = jwt.verify(token, jwt_secret);
    const user = await Users.findOne({ _id: verified._id });
    if (!user) return createError(next, 404, "Resource not found");
    req.user = user;
    next();
  } catch (error) {
    next(error);
  }
};
